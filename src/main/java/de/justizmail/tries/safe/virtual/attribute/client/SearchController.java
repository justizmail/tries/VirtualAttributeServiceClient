/*
 *
 * Copyright (C) 2018  Frank Engler
 *
 * This file is part of Virtual AttributeService Client.
 *
 * Virtual AttributeService Client is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Virtual AttributeService Client is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.justizmail.tries.safe.virtual.attribute.client;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import oasis.names.tc.spml._2._0.search.ObjectFactory;
import oasis.names.tc.spml._2._0.search.SearchResponseType;

public class SearchController {

    private String searchTerm;

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public String getSearchResponse() {
        if (null != getSearchTerm() &&  !getSearchTerm().isEmpty()) {
            AllKeySearch search = new AllKeySearch(getSearchTerm());
            StringWriter stringWriter = new StringWriter();
            search.call().ifPresent(searchResponse-> {
                try {
                    JAXBContext jaxbContext = JAXBContext.newInstance(SearchResponseType.class);
                    Marshaller marshaller = jaxbContext.createMarshaller();
                    marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    marshaller.marshal(new ObjectFactory().createSearchResponse(searchResponse), stringWriter);
                } catch (JAXBException e) {}
            });
            return stringWriter.toString();
        }
        return "";
    }
}