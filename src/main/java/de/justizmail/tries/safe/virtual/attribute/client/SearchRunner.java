/*
 *
 * Copyright (C) 2018  Frank Engler
 *
 * This file is part of Virtual AttributeService Client.
 *
 * Virtual AttributeService Client is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Virtual AttributeService Client is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.justizmail.tries.safe.virtual.attribute.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import de.safe.attribute.AttributeServicePortType;
import oasis.names.tc.spml._2._0.ExecutionModeType;
import oasis.names.tc.spml._2._0.NamespacePrefixMappingType;
import oasis.names.tc.spml._2._0.ObjectFactory;
import oasis.names.tc.spml._2._0.ReturnDataType;
import oasis.names.tc.spml._2._0.SelectionType;
import oasis.names.tc.spml._2._0.StatusCodeType;
import oasis.names.tc.spml._2._0.search.ScopeType;
import oasis.names.tc.spml._2._0.search.SearchQueryType;
import oasis.names.tc.spml._2._0.search.SearchRequestType;
import oasis.names.tc.spml._2._0.search.SearchResponseType;

class SearchRunner implements Callable<Optional<SearchResponseType>> {
    private final String WSDL_DOCUMENT_LOCATION = "http://vas.bnotk.de/SAFE-Attribute/AttributeServiceOpenSearchV2?wsdl";
    private final QName SERVICE_NAME = new QName("http://attribute.safe.de/", "AttributeServiceOpenSearchV2");

    private Service service;
    private AttributeServicePortType port;

    private String searchTerm;
    private String searchKey;

    SearchRunner() {}

    SearchRunner(String searchTerm, String searchKey) {
        this();
        setSearchTerm(searchTerm);
        setSearchKey(searchKey);
    }

    void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    String getSearchTerm() {
        return searchTerm;
    }

    void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    String getSearchKey() {
        return searchKey;
    }

    private SelectionType generateSelection() {
        SelectionType selection = new SelectionType();
        selection.setPath(String.format(SearchKeys.getSearchPattern(getSearchKey()), getSearchTerm()));
        selection.setNamespaceURI("http://www.w3.org/TR/xpath20/");
        List<NamespacePrefixMappingType> namespacePrefixMaps = selection.getNamespacePrefixMap();
        Map<String, String> namespaces = SearchKeys.getNamespaces(getSearchKey());
        namespaces.forEach((key, value) -> {
            NamespacePrefixMappingType namespacePrefixMap = new NamespacePrefixMappingType();
            namespacePrefixMap.setPrefix(key);
            namespacePrefixMap.setNamespace(value);
            namespacePrefixMaps.add(namespacePrefixMap);
        });
        return selection;
    }

    private SearchQueryType generateSearchQuery() {
        SearchQueryType searchQuery = new SearchQueryType();
        List<Object> selections = searchQuery.getAny();
        selections.add(new ObjectFactory().createSelect(generateSelection()));

        /*
         * The contract does not require a targetID attribute, but according to section 6.2.2 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * AttributeService works only on targetID="urn:de:egov:names:safe:1.0:spml:2.0:targetid:as:egvpdata:id-sis-pp".
         * All the examples in https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * use targetID="urn:de:egov:names:safe:1.0:spml:2.0:targetid:egvpdata:id-sis-pp".
         * TODO: What does ListsTargets say? Should we use the response of ListsTargets dynamically?
         */
        searchQuery.setTargetID("urn:de:egov:names:safe:1.0:spml:2.0:targetid:egvpdata:id-sis-pp");

        /*
         * The contract does not require a scope attribute, but according to section 6.2.2.3 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * the only allowed scopes are scope=’oneLevel’ or scope=’subTree’. Note: both options mean
         * all data.
         */
        searchQuery.setScope(ScopeType.SUB_TREE);
        return searchQuery;
    }

    private SearchRequestType generateSearchRequest() {
        SearchRequestType searchRequest = new SearchRequestType();
        searchRequest.setQuery(generateSearchQuery());

        /*
         * The contract does not require a returnData attribute, but according to section 6.2.2.3 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * only returnData="everything" is allowed.
         */
        searchRequest.setReturnData(ReturnDataType.EVERYTHING);

        /*
         * The contract does not require a requestID attribute, but according to section 6.2.2 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * a random requestID is required.
         * The contract requires a type of xsd:ID. So we prepend "uuid." to a random UUID, which may
         * be uncommon but well within the specs.
         */
        searchRequest.setRequestID("uuid." + UUID.randomUUID());

        /*
         * The contract does not require an executionMode attribute, but according to section 6.2.2 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * executionMode="synchronous" is required.
         */
        searchRequest.setExecutionMode(ExecutionModeType.SYNCHRONOUS);
        return searchRequest;
    }

    @Override
    public Optional<SearchResponseType> call() {
        try {
            final URL wsdlDocumentLocation = new URL(WSDL_DOCUMENT_LOCATION);
            service = Service.create(wsdlDocumentLocation, SERVICE_NAME);
            port = service.getPort(AttributeServicePortType.class);
            SearchResponseType searchResponse = port.search(generateSearchRequest());
            if (StatusCodeType.SUCCESS == searchResponse.getStatus()) {
                return Optional.of(searchResponse);
            }
        // We use a hard-coded URL string so this should never happen.
        } catch (MalformedURLException e) {}
        return Optional.empty();
    }
}