/*
 *
 * Copyright (C) 2018  Frank Engler
 *
 * This file is part of Virtual AttributeService Client.
 *
 * Virtual AttributeService Client is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Virtual AttributeService Client is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.justizmail.tries.safe.virtual.attribute.client;

import java.util.HashMap;
import java.util.Map;

class SearchKeys {
    static final String ALL_DATA = "all";
    static final String FORM_OF_ADDRESS = "FormOfAddress";
    static final String PERSONAL_TITLE = "PersonalTitle";
    static final String FIRST_NAME = "FN";
    static final String SURNAME = "SN";
    static final String POSTAL_CODE = "PostalCode";
    static final String LOCALITY_NAME ="L";
    static final String STATE_NAME = "St";
    static final String COUNTRY_CODE = "C";
    static final String STREET_NAME = "StreetName";
    static final String HOUSE_NUMBER = "HouseNumber";
    static final String TELEPHONE_NUMBER = "pots";
    static final String EMAIL = "email";
    static final String DEMAIL = "deMail";
    static final String MOBILE = "cellPhone";
    static final String FACSIMILE_TELEPHONE_NUMBER = "fax";
//    static final String INTERMED_URL = "IntermedAddress";
//    static final String INTERMED_ENCRYPT_KEY = "IntermedEncryptKey";
//    static final String RECIPIENT_ENCRYPT_KEY = "RecipientEncryptKey";
    static final String ORGANIZATION = "Organization";
    static final String ALTERNATIVE_ORGANIZATION = "AlternativeOrganization";
    static final String REFERENCE_NUMBER = "ReferenceNumber";
    static final String REFERENCE_NUMBER_PREFIX = "ReferenceNumberPrefix";
    static final String EXTERNAL_ID = "ExternalID";
    static final String ACCOUNT_GROUP = "AccountGroup";
    static final String ROLE_NAME = "RoleName";
    static final String ROLE_VALUE = "RoleValue";
    static final String ROLE_ID = "RoleID";
    static final String USER_PRINCIPAL_NAME ="Username";
    static final String IDENTIFICATION_TYPE = "IdentificationType";

    private static final Map<String, String> NAMESPACE_PP = new HashMap<String, String>(4);
    private static final Map<String, String> NAMESPACE_FIM = new HashMap<String, String>(4);
    private static final Map<String, String> NAMESPACE_SAFE = new HashMap<String, String>(4);

    private static Map<String, String> searchPatterns = new HashMap<String, String>(48);
    private static Map<String, Map<String, String>> namespaces = new HashMap<String, Map<String, String>>(48);

    static {
        NAMESPACE_PP.put("pp", "urn:liberty:id-sis-pp:2005-05");
        NAMESPACE_FIM.putAll(NAMESPACE_PP);
        /*
         * According to section 6.2.2.3 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * we are required to declare any used name space. Neither the contract nor
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * require a specific name space prefix. But we do not get any result if the name space
         * prefix is anything other than fim.
         */
        NAMESPACE_FIM.put("fim", "urn:de:egov:names:fim:1.0:id-sis-pp:extension");

        NAMESPACE_SAFE.putAll(NAMESPACE_PP);
        /*
         * According to section 6.2.2.3 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * we are required to declare any used name space. Neither the contract nor
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * require a specific name space prefix. But we do not get any result if the name space
         * prefix is anything other than safe.
         */
        NAMESPACE_SAFE.put("safe", "urn:de:egov:names:safe:1.0:id-sis-pp:extension");

        searchPatterns.put(ALL_DATA, "//pp:PP");
        namespaces.put(ALL_DATA, NAMESPACE_PP);

        searchPatterns.put(FORM_OF_ADDRESS, "//pp:PP[fn:contains(pp:CommonName/pp:AnalyzedName/pp:Extension/fim:FormOfAddress, '%s')]");
        namespaces.put(FORM_OF_ADDRESS, NAMESPACE_FIM);

        searchPatterns.put(PERSONAL_TITLE, "//pp:PP[fn:contains(pp:CommonName/pp:AnalyzedName/pp:PersonalTitle, '%s')]");
        namespaces.put(PERSONAL_TITLE, NAMESPACE_PP);

        searchPatterns.put(FIRST_NAME, "//pp:PP[fn:contains(pp:CommonName/pp:AnalyzedName/pp:FN, '%s')]");
        namespaces.put(FIRST_NAME, NAMESPACE_PP);

        searchPatterns.put(SURNAME, "//pp:PP[fn:contains(pp:CommonName/pp:AnalyzedName/pp:SN, '%s')]");
        namespaces.put(SURNAME, NAMESPACE_PP);

        searchPatterns.put(POSTAL_CODE, "//pp:PP[fn:contains(pp:AddressCard/pp:Address/pp:PostalCode, '%s')]");
        namespaces.put(POSTAL_CODE, NAMESPACE_PP);

        searchPatterns.put(LOCALITY_NAME, "//pp:PP[fn:contains(pp:AddressCard/pp:Address/pp:L, '%s')]");
        namespaces.put(LOCALITY_NAME, NAMESPACE_PP);

        searchPatterns.put(STATE_NAME, "//pp:PP[fn:contains(pp:AddressCard/pp:Address/pp:St, '%s')]");
        namespaces.put(STATE_NAME, NAMESPACE_PP);

        searchPatterns.put(COUNTRY_CODE, "//pp:PP[fn:contains(pp:AddressCard/pp:Address/pp:C, '%s')]");
        namespaces.put(COUNTRY_CODE, NAMESPACE_PP);

        searchPatterns.put(STREET_NAME, "//pp:PP[fn:contains(pp:AddressCard/pp:Address/pp:Extension/fim:StreetName, '%s')]");
        namespaces.put(STREET_NAME, NAMESPACE_FIM);

        /*
         * According to section 6.2.2.3 of
         * https://justiz.de/elektronischer_rechtsverkehr/grob-und-feinkonzept/SAFE_Fachkonzept.pdf
         * contains() is a supported operation. But we get all data if we use
         * contains(HouseNumber, %s) with anything but a number.
         */
        searchPatterns.put(HOUSE_NUMBER, "//pp:PP[pp:AddressCard/pp:Address/pp:Extension/fim:HouseNumber='%s']");
        namespaces.put(HOUSE_NUMBER, NAMESPACE_FIM);

        searchPatterns.put(TELEPHONE_NUMBER, "//pp:PP[fn:contains(pp:MsgContact[pp:MsgTechnology='urn:liberty:id-sis-pp:msgTechnology:pots']/pp:MsgAccount, '%s')]");
        namespaces.put(TELEPHONE_NUMBER, NAMESPACE_PP);

        searchPatterns.put(EMAIL, "//pp:PP[fn:contains(pp:MsgContact[pp:MsgTechnology='urn:liberty:id-sis-pp:msgTechnology:email']/pp:MsgAccount, '%s')]");
        namespaces.put(EMAIL, NAMESPACE_PP);

        searchPatterns.put(DEMAIL, "//pp:PP[fn:contains(pp:MsgContact[pp:MsgTechnology='urn:de:egov:names:fim:1.0:id-sis-pp:msgTechnology:deMail']/pp:MsgAccount, '%s')]");
        namespaces.put(DEMAIL, NAMESPACE_PP);

        searchPatterns.put(MOBILE, "//pp:PP[fn:contains(pp:MsgContact[pp:MsgTechnology='urn:de:egov:names:fim:1.0:id-sis-pp:msgTechnology:cellPhone']/pp:MsgAccount, '%s')]");
        namespaces.put(MOBILE, NAMESPACE_PP);

        searchPatterns.put(FACSIMILE_TELEPHONE_NUMBER, "//pp:PP[fn:contains(pp:MsgContact[pp:MsgTechnology='urn:liberty:id-sis-pp:msgTechnology:fax']/pp:MsgAccount, '%s')]");
        namespaces.put(FACSIMILE_TELEPHONE_NUMBER, NAMESPACE_PP);

//        searchPatterns.put(INTERMED_URL, "");
//        namespaces.put(INTERMED_URL, NAMESPACE_SAFE);
//
//        searchPatterns.put(INTERMED_ENCRYPT_KEY, "");
//        namespaces.put(INTERMED_ENCRYPT_KEY, NAMESPACE_SAFE);
//
//        searchPatterns.put(RECIPIENT_ENCRYPT_KEY, "");
//        namespaces.put(RECIPIENT_ENCRYPT_KEY, NAMESPACE_SAFE);

        searchPatterns.put(ORGANIZATION, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:Organization, '%s')]");
        namespaces.put(ORGANIZATION, NAMESPACE_SAFE);

        searchPatterns.put(ALTERNATIVE_ORGANIZATION, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:AlternativeOrganization, '%s')]");
        namespaces.put(ALTERNATIVE_ORGANIZATION, NAMESPACE_SAFE);

        searchPatterns.put(REFERENCE_NUMBER, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:ReferenceNumber, '%s')]");
        namespaces.put(REFERENCE_NUMBER, NAMESPACE_SAFE);

        searchPatterns.put(REFERENCE_NUMBER_PREFIX, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:ReferenceNumberPrefix, '%s')]");
        namespaces.put(REFERENCE_NUMBER_PREFIX, NAMESPACE_SAFE);

        searchPatterns.put(EXTERNAL_ID, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:ExternalID, '%s')]");
        namespaces.put(EXTERNAL_ID, NAMESPACE_SAFE);

        searchPatterns.put(ACCOUNT_GROUP, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:AccountGroup, '%s')]");
        namespaces.put(ACCOUNT_GROUP, NAMESPACE_SAFE);

        searchPatterns.put(ROLE_NAME, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:Roles/safe:RoleName, '%s')]");
        namespaces.put(ROLE_NAME, NAMESPACE_SAFE);

        searchPatterns.put(ROLE_VALUE, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:Roles/safe:RoleValue, '%s')]");
        namespaces.put(ROLE_VALUE, NAMESPACE_SAFE);

        searchPatterns.put(ROLE_ID, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:RoleID, '%s')]");
        namespaces.put(ROLE_ID, NAMESPACE_SAFE);

        searchPatterns.put(USER_PRINCIPAL_NAME, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:/Username, '%s')]");
        namespaces.put(USER_PRINCIPAL_NAME, NAMESPACE_SAFE);

        searchPatterns.put(IDENTIFICATION_TYPE, "//pp:PP[fn:contains(pp:Extension/safe:EJusticeAttributes/safe:IdentificationType, '%s')]");
        namespaces.put(IDENTIFICATION_TYPE, NAMESPACE_SAFE);
    }

    private SearchKeys() {}

    static String getSearchPattern(String searchKey) {
        return searchPatterns.get(searchKey);
    }

    static Map<String, String> getNamespaces(String searchKey) {
        return namespaces.get(searchKey);
    }
}