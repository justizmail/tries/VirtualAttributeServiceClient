/*
 *
 * Copyright (C) 2018  Frank Engler
 *
 * This file is part of Virtual AttributeService Client.
 *
 * Virtual AttributeService Client is free software: you can redistribute it
 * and/or modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Virtual AttributeService Client is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.justizmail.tries.safe.virtual.attribute.client;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import oasis.names.tc.spml._2._0.PSOType;
import oasis.names.tc.spml._2._0.search.SearchResponseType;

class AllKeySearch {

    private String searchTerm;

    AllKeySearch() {}

    AllKeySearch(String searchTerm) {
        this();
        setSearchTerm(searchTerm);
    }

    void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    String getSearchTerm() {
        return searchTerm;
    }

    public Optional<SearchResponseType> call() {
        ExecutorService executor = Executors.newCachedThreadPool();
        Set<SearchRunner> searchRunners = new HashSet<SearchRunner>();
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.FORM_OF_ADDRESS));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.PERSONAL_TITLE));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.FIRST_NAME));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.SURNAME));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.POSTAL_CODE));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.LOCALITY_NAME));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.STATE_NAME));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.COUNTRY_CODE));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.STREET_NAME));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.HOUSE_NUMBER));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.TELEPHONE_NUMBER));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.EMAIL));
//        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.DEMAIL));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.MOBILE));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.FACSIMILE_TELEPHONE_NUMBER));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.ORGANIZATION));
//        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.ALTERNATIVE_ORGANIZATION));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.REFERENCE_NUMBER));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.REFERENCE_NUMBER_PREFIX));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.EXTERNAL_ID));
//        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.ACCOUNT_GROUP));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.ROLE_NAME));
        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.ROLE_VALUE));
//        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.ROLE_ID));
//        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.USER_PRINCIPAL_NAME));
//        searchRunners.add(new SearchRunner(getSearchTerm(), SearchKeys.IDENTIFICATION_TYPE));

        SearchResponseType returnValue = null;
        try {
            Map<String, PSOType> psos = new ConcurrentHashMap<String, PSOType>();
            executor.invokeAll(searchRunners).forEach(futureSearchResponse -> {
                try {
                    futureSearchResponse.get().ifPresent(searchResponse -> {
                        if (null != searchResponse.getPso()) {
                            searchResponse.getPso().forEach(pso -> psos.put(pso.getPsoID().getID(), pso));
                        }
                    });
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
            if (!psos.isEmpty()) {
                returnValue = new SearchResponseType();
                returnValue.getPso().addAll(psos.values());
            }
        } catch (InterruptedException e) {} finally {
            executor.shutdown();
        }
        return Optional.ofNullable(returnValue);
    }
}