<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--

Copyright (C) 2018  Frank Engler

This file is part of Virtual AttributeService Client.

Virtual AttributeService Client is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of the
License, or any later version.

Virtual AttributeService Client is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.

 -->

<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.3">
  <jsp:directive.page contentType="application/xhtml+xml; charset=UTF-8" pageEncoding="UTF-8" session="false"/>
  <jsp:output doctype-root-element="html" doctype-system="about:legacy-compat"/>
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <title>Virtual AttributeService Client</title>
      <link rel="stylesheet" href="style.css" media="all"/>
    </head>
    <body>
      <header>
        <h1>Virtual AttributeService Client</h1>
      </header>
      <main>
        <article>
          <p>Clients zum Virtual AttributeService:</p>
          <ul>
            <li>
              <a href="search.jsp" title="Suche">Search</a>
            </li>
          </ul>
        </article>
      </main>
      <footer>
        <ul>
          <li>
            <a href="https://gitlab.com/justizmail/tries/VirtualAttributeServiceClient" title="Quellcode auf GitLab">Code auf GitLab</a>
          </li>
        </ul>
      </footer>
    </body>
  </html>
</jsp:root>