<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--

Copyright (C) 2018  Frank Engler

This file is part of Virtual AttributeService Client.

Virtual AttributeService Client is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of the
License, or any later version.

Virtual AttributeService Client is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xhtml="http://www.w3.org/1999/xhtml">
  <xsl:output method="xml"/>
  <xsl:template match="/xhtml:html">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
      <xsl:copy-of select="xhtml:head"/>
      <body>
        <xsl:copy-of select="xhtml:body/xhtml:header"/>
        <main>
          <xsl:copy-of select="xhtml:body/xhtml:main/xhtml:article[@id='search']"/>
          <xsl:copy-of select="xhtml:body/xhtml:main/xhtml:article[@id='searchResponse']"/>
        </main>
        <xsl:copy-of select="xhtml:body/xhtml:footer"/>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>