<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!--

Copyright (C) 2018  Frank Engler

This file is part of Virtual AttributeService Client.

Virtual AttributeService Client is free software: you can redistribute it
and/or modify it under the terms of the GNU Affero General Public License
as published by the Free Software Foundation, either version 3 of the
License, or any later version.

Virtual AttributeService Client is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.

 -->

<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.3">
  <jsp:directive.page contentType="application/xhtml+xml; charset=UTF-8" pageEncoding="UTF-8" session="false"/>
  <jsp:output doctype-root-element="html" doctype-system="about:legacy-compat"/>
  <![CDATA[<?xml-stylesheet type='text/xsl' href='transform.xsl'?>
  ]]>
  <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
    <head>
      <title>Search | Virtual AttributeService Client</title>
      <link rel="stylesheet" href="style.css" media="all"/>
    </head>
    <body>
      <header>
        <h1>Virtual AttributeService Client</h1>
      </header>
      <main>
        <article id="search">
          <form method="post" action="search.jsp" accept-charset="utf-8">
            <input type="text" name="searchTerm" title="Suchbegriff" size="60" maxlength="255" required="required"/><br/>
            <input type="submit" name="submit" value="Suche starten"/>
          </form>
        </article>
        <article id="searchResponse">
          <jsp:useBean id="searchController" scope="page" class="de.justizmail.tries.safe.virtual.attribute.client.SearchController"/>
          <jsp:setProperty name="searchController" property="searchTerm"/>
          <jsp:getProperty name="searchController" property="searchResponse"/>
        </article>
      </main>
      <footer>
        <ul>
          <li>
            <a href="https://gitlab.com/justizmail/tries/VirtualAttributeServiceClient" title="Quellcode auf GitLab">Code auf GitLab</a>
          </li>
        </ul>
      </footer>
    </body>
  </html>
</jsp:root>